//
//  WorkoutView.swift
//  Parkour WatchKit Extension
//
//  Created by Davide Monsurrocco on 23/01/2020.
//  Copyright © 2020 University of Naples Federico II. All rights reserved.
//

import Foundation
import SwiftUI

struct WorkoutView: View {
    
    
    @ObservedObject var alti = altituteData()
    @ObservedObject var watch = StopWatch()
    
    
    
    var body: some View {
        
        VStack(spacing: 2.0) {
            
            

                Text("Time")
                .foregroundColor(.orange)
                .font(.system(size: 16.5))
                .padding(.bottom, -100.0)
                .padding(.top,10)
                .padding(.leading, -73.0)
              
            
            
            
            Text(watch.stopWatchTime).font(.system(size: 41.5))
                .padding(.top,10)
                .padding(.bottom,13)
 
                Text("Altitude")
                .foregroundColor(.orange)
                .font(.system(size: 16.5))
                .padding(.leading, -73.0)
                .padding(.top, 10.0)
                .padding(.bottom,-11)
            
            
            HStack(spacing: 38.0) {
                
                HStack() {
                    
                    Text("\(alti.altiField)")
                        
                    
                    
                    Text("m")
                        .font(.system(size: 16.0))
                        .padding(.top,20)
                    
                }
                
                Button(action: {
                    print("button pressed")
                }) {
                    
                    Image("lungs").resizable().scaledToFit()

                }.buttonStyle(PlainButtonStyle())
                
                
            }
            
            
            
        }.navigationBarTitle("Parkour").onAppear {
            
        
            print("timer started")
                
                self.watch.start()
                
        }.onDisappear() {
            
            print("timer reset")
            self.watch.reset()
            
            
        }
    }
    
    struct WorkoutView_Previews: PreviewProvider {
        static var previews: some View {
            WorkoutView()
        }
    }
}
