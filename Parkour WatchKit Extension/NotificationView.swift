//
//  NotificationView.swift
//  Parkour WatchKit Extension
//
//  Created by Alessandro Barruffo on 20/01/2020.
//  Copyright © 2020 University of Naples Federico II. All rights reserved.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
