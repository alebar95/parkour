//
//  ContentView.swift
//  Parkour WatchKit Extension
//
//  Created by Alessandro Barruffo on 20/01/2020.
//  Copyright © 2020 University of Naples Federico II. All rights reserved.
//

import SwiftUI

/*
struct ListView: View {
    var body: some View {
        List{

            RowView(title: "Workout")
            RowView(title: "Speed")

    }
//.listStyle(.CarouselListStyle)


    }
}
struct RowView: View {
    let title: String
    var body: some View {

    NavigationLink(destination: Text("Detail \(title)")) {

        Text(title)
            .frame(height: 80, alignment: .topTrailing)
            .listRowBackground(
                Color.blue
                    .cornerRadius(12)
        )
    }
    }
}
*/

struct ListView: View {
    
    
     @ObservedObject var watch = StopWatch()
    
    

    var body: some View {

        VStack(spacing: 18.0) {
     
            
            NavigationLink(destination: WorkoutView()) {
                
               
                Text("Workout")
                    .foregroundColor(Color.orange)
                .fontWeight(.semibold)
                    .padding().overlay(
                        RoundedRectangle(cornerRadius: 40)                         .stroke(Color.orange, lineWidth: 1)
                            .padding(.trailing,-38)
                            .padding(.leading,-37)
                            
                    )
                
            }.buttonStyle(PlainButtonStyle())
            
            
            NavigationLink(destination: SpeedView()) {
                         
                     
                         
                         
                         Text("Speed")
                             .foregroundColor(Color.orange)
                                     .fontWeight(.semibold)
                                         .padding().overlay(
                                             RoundedRectangle(cornerRadius: 40)                         .stroke(Color.orange, lineWidth: 1)
                                                 .padding(.trailing,-46)
                                                 .padding(.leading,-46)
                                                 
                                         )
                                     

                         
                         
                         
                     }.buttonStyle(PlainButtonStyle())
        

        }.navigationBarTitle("Parkour")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ListView()
    }
}
/*
struct List_Previews: PreviewProvider {
    static var previews: some View {
        ListView()
    }
}
*/
