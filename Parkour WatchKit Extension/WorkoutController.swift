//
//  WorkoutController.swift
//  Parkour WatchKit Extension
//
//  Created by Davide Monsurrocco on 23/01/2020.
//  Copyright © 2020 University of Naples Federico II. All rights reserved.
//

import Foundation
import WatchKit
import Foundation
import SwiftUI

class WorkoutController: WKHostingController<WorkoutView> {
    override var body: WorkoutView {
        return WorkoutView()
    }
}
