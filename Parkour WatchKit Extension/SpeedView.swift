//
//  SpeedView.swift
//  Parkour WatchKit Extension
//
//  Created by Alessandro Barruffo on 22/01/2020.
//  Copyright © 2020 University of Naples Federico II. All rights reserved.
//

import SwiftUI

struct SpeedView: View {
    
    
    
    @State var timeRemaining = 70
    
    
    var timer: Timer {
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) {_ in
            if self.timeRemaining > 0 {
                self.timeRemaining -= 1
            }
        }
    }
    
    
    var body: some View {
        
        
        VStack(spacing: 2.0) {
            
            
            Text("\(timeRemaining)").onAppear(perform: {
                let _ = self.timer
            }).font(.custom("HelveticaNeue", size: 50))
            
        }.navigationBarTitle("Parkour").onDisappear() {
            
            self.timeRemaining = 70
            
        }
        
        
    }
    
    struct SpeedView_Previews: PreviewProvider {
        static var previews: some View {
            SpeedView()
        }
    }
}
