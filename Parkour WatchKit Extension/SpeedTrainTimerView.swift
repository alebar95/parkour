import SwiftUI

struct SpeedTrainTimerView: View {
    @State var timeRemaining = 70
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    
    var body: some View {
        
        
        
        
        
        Text("\(timeRemaining)")
            .onReceive(timer) { _ in
                if self.timeRemaining > 0 {
                    self.timeRemaining -= 1
                }
        }
        .font(.custom("HelveticaNeue", size: 50))
    }
}

struct SpeedTrainTimerView_Previews: PreviewProvider {
    static var previews: some View {
        SpeedTrainTimerView()
    }
}
