//
//  SpeedController.swift
//  Parkour WatchKit Extension
//
//  Created by Davide Monsurrocco on 23/01/2020.
//  Copyright © 2020 University of Naples Federico II. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class SpeedController: WKHostingController<SpeedView> {
    override var body: SpeedView {
        return SpeedView()
    }
}
