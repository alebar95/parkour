//
//  HostingController.swift
//  Parkour WatchKit Extension
//
//  Created by Alessandro Barruffo on 20/01/2020.
//  Copyright © 2020 University of Naples Federico II. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<ListView> {
    override var body: ListView {
        return ListView()
    }
}
