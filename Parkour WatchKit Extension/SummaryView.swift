//
//  SummaryView.swift
//  Parkour WatchKit Extension
//
//  Created by Alessandro Barruffo on 22/01/2020.
//  Copyright © 2020 University of Naples Federico II. All rights reserved.
//

import SwiftUI






struct summaryItem : View {
    
    
    
    
    
    var body : some View {
        
        
        VStack(alignment : .leading, spacing : 5.0) {
            
            Text("Time & Dist").foregroundColor(Color.yellow).fontWeight(.semibold)
            
            
            VStack(alignment: .leading){
                
                

                
                HStack(spacing: 25) {
                    
                       Text("Time : ")
                    
                      Text("00:15:00")
                    
                }
                
                HStack(spacing: 27) {
                    
                     Text("Distance :")
                    
                    Text("0 km")
                    
                }
             
               
                
                
            }
            
             
            
        
            
            
        }
        
    
        
        
        
    }
    
    
}



struct SummaryView: View {
    var body: some View {
        
        
        List(){
      
            
            summaryItem()
            summaryItem()
            
            /*
            Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/).foregroundColor(.orange).overlay(
                RoundedRectangle(cornerRadius: 20)
                    .stroke(Color.orange, lineWidth: 1)
                    .padding(.trailing, -75).padding(.top,-10).padding(.leading,-10).padding(.bottom,-10)
                   
            )
            
           
            
            Text("Ciao Belli")
            */
        }
        
        
        
        
        
    }
}

struct SummaryView_Previews: PreviewProvider {
    static var previews: some View {
        SummaryView()
    }
}
