//
//  SensorDataView.swift
//  Parkour WatchKit Extension
//
//  Created by Alessandro Barruffo on 22/01/2020.
//  Copyright © 2020 University of Naples Federico II. All rights reserved.
//


import SwiftUI
import Combine
import CoreMotion

class altituteData : ObservableObject {
    //@Published var relativeAltitude : NSNumber = 0
    
    public let objectWillChange = PassthroughSubject<CMAltimeter,Never>()
    public private(set) var altiField: CMAltimeter = CMAltimeter() {
        willSet {
            objectWillChange.send(newValue)
        }
    }
    deinit {
        altiField.stopRelativeAltitudeUpdates()
    }
    private let altiManager: CMAltitudeData

    public init(){
        self.altiManager = CMAltitudeData()
    }

    public func startUpdates() {
        altiField.startRelativeAltitudeUpdates(to: OperationQueue.main) { this, that in
            return self.altiManager.relativeAltitude
            }
        }
    }



public class MagneticFieldProvider: NSObject, ObservableObject {

    public let objectWillChange = PassthroughSubject<CMMagneticField,Never>()
    public private(set) var magneticField: CMMagneticField = CMMagneticField() {
        willSet {
            objectWillChange.send(newValue)
        }
    }
    deinit {
        motionManager.stopMagnetometerUpdates()
    }
    private let motionManager: CMMotionManager

    public override init(){
        self.motionManager = CMMotionManager()
        super.init()
        motionManager.magnetometerUpdateInterval = 1/60
    }

    public func startUpdates() {
        motionManager.startMagnetometerUpdates(to: OperationQueue.main) { this, that in
            if let magneticField = self.motionManager.magnetometerData?.magneticField {
                self.magneticField = magneticField
            }
        }
    }
}

public class GyroFieldProvider: NSObject, ObservableObject {

    public let objectWillChange = PassthroughSubject<CMRotationRate,Never>()
    public private(set) var gyroFields: CMRotationRate = CMRotationRate() {
        willSet {
            objectWillChange.send(newValue)
        }
    }
    deinit {
        motionManager.stopGyroUpdates()
    }
    private let motionManager: CMMotionManager

    public override init(){
        self.motionManager = CMMotionManager()
        super.init()
        motionManager.gyroUpdateInterval = 1/60
    }

    public func startUpdates() {
        motionManager.startGyroUpdates(to: OperationQueue.main) { this, that in
            if let gyroField = self.motionManager.gyroData?.rotationRate {
                self.gyroFields.x = gyroField.x
                self.gyroFields.y = gyroField.y
                self.gyroFields.z = gyroField.z
            }
        }
    }
}

public class AccelerometerFieldProvider: NSObject, ObservableObject {

    public let objectWillChange = PassthroughSubject<CMAcceleration,Never>()
    public private(set) var accelerometerField: CMAcceleration = CMAcceleration() {
        willSet {
            objectWillChange.send(newValue)
        }
    }
    deinit {
        motionManager.stopAccelerometerUpdates()
    }
    private let motionManager: CMMotionManager

    public override init(){
        self.motionManager = CMMotionManager()
        super.init()
        motionManager.accelerometerUpdateInterval = 1/60
    }

    public func startUpdates() {
        motionManager.startAccelerometerUpdates(to: OperationQueue.main) { this, that in
            if let accelerometerField = self.motionManager.accelerometerData?.acceleration {
                self.accelerometerField.x = accelerometerField.x
                self.accelerometerField.y = accelerometerField.y
                self.accelerometerField.z = accelerometerField.z
            }
        }
    }
}

//public class AltimeterFieldProvider: NSObject, ObservableObject {
//
//    public let objectWillChange = PassthroughSubject<CMAltitudeData,Never>()
//    public private(set) var altitudeField: CMAltitudeData = CMAltitudeData() {
//        willSet {
//            objectWillChange.send(newValue)
//        }
//    }
//
//    private let motionManager: CMAltimeter
//    private let altiManager: CMAltitudeData
//
//    public override init(){
//        self.motionManager = CMAltimeter()
//        self.altiManager = CMAltitudeData()
//        super.init()
//    }
//
//    public func startUpdates() {
//        motionManager.startRelativeAltitudeUpdates(to: OperationQueue.main) { this, that in
//            let altiField = self.altiManager.relativeAltitude
//
//            altituteData().relativeAltitude = altiField
//
//            }
//        }
//}


struct SensorDataView: View {
    @ObservedObject var magnetometer = MagneticFieldProvider()
    @ObservedObject var gyroscope = GyroFieldProvider()
    @ObservedObject var accelerometer = AccelerometerFieldProvider()
//    @ObservedObject var altimeter = AltimeterFieldProvider()
    @ObservedObject var altitude = altituteData()

    var body: some View {
        
        List{
            Text("Altimeter Data")
            Text("Altitude: \(altitude.altiField)")
            Text("Magnetometer Data")
            Text("X: \(magnetometer.magneticField.x)")
            Text("Y: \(magnetometer.magneticField.y)")
            Text("Z: \(magnetometer.magneticField.z)")
        }.onAppear(perform: { self.magnetometer.startUpdates() })
//        HStack{
//
//            VStack(alignment: .leading){
//            Text("Altimeter Data")
//            Text("Altitude: \(altitude.relativeAltitude)")
//            }
//
//            VStack(alignment: .leading) {
//            Text("Magnetometer Data")
//            Text("X: \(magnetometer.magneticField.x)")
//            Text("Y: \(magnetometer.magneticField.y)")
//            Text("Z: \(magnetometer.magneticField.z)")
//        }   .onAppear(perform: { self.magnetometer.startUpdates() })
//
//        VStack(alignment: .leading) {
//            Text("Gyroscope Data")
//            Text("X: \(gyroscope.gyroFields.x)")
//            Text("Y: \(gyroscope.gyroFields.y)")
//            Text("Z: \(gyroscope.gyroFields.z)")
//        }   .onAppear(perform: { self.gyroscope.startUpdates() })
//
//        VStack(alignment: .leading) {
//            Text("Accelerometer Data")
//            Text("X: \(accelerometer.accelerometerField.x)")
//            Text("Y: \(accelerometer.accelerometerField.y)")
//            Text("Z: \(accelerometer.accelerometerField.z)")
//            }   .onAppear(perform: { self.accelerometer.startUpdates() })
//        }
    }
}





struct SensorDataView_Previews: PreviewProvider {
    static var previews: some View {
        SensorDataView()
    }
}
